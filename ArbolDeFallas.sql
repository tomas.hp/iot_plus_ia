
create table ArbolDeFallas (
idFS int auto_increment,
nomFallaSolucion char(255) unique,
fallaSoln boolean,
idFalla int,
primary key (idFS)
);

create table SolnSimilares(
idSoln int,
idFalla int,
foreign key (idSoln) references ArbolDeFallas(idFS),
foreign key (idFalla) references ArbolDeFallas(idFS),
primary key (idSoln,idFalla) 
);


--
-- INSERTS
--

-- Nombre Alumno: Francisco Gutierrez Mujica
-- <FGM>
insert into ArbolDeFallas values (1, 'Se calientea la PC', true, null),
                                 (2, 'Están obstruidos los disipadores',false,1),
								 (3,"verifcar pasta termica",false,1);

-- </FGM>


--
-- INSERTS
--

-- Nombre: Raul Salazar Bahena
-- <RSB>
insert into ArbolDeFallas (nomFallaSolucion, fallaSoln, idFalla) 
						  values ('El teclado no funciona', true, null),
								 ('Existe un exceso de suciedad', false, 46),
                                 ('Hacerle una limpieza al teclado', false, 47),
                                 ('No detecta los controladores', false, 46),
                                 ('Teclado desgastado por el uso', false, 46);
-- <RSB>
        
        
        
-- Nombre: Alex Lozano García
-- <ALG>
insert into ArbolDeFallas (nomFallaSolucion, fallaSoln, idFalla) 
						  values ('Laptop no carga', true, null),
								 ('Verifica que el enchufe funciona correctamente', false, 64),
								 ('Retira y vuelve a colocar la bateria (Si es posible)', false, 64),
                                 ('Prueba con otro adaptador de corriente', false, 64),
                                 ('Limpia el puerto de carga o los contactos de corriente', false, 64),
                                 ('Cable de carga dañado', false, 64),
                                 ('Puerto de alimentacion dañado', false, 64),
                                 ('Bateria dañada', false, 64),
                                 ('Reemplazo de bateria', false, 71);
-- <ALG>


                                 
-- Nombre: Antonio Flores Moreno
-- <AFM>
insert into ArbolDeFallas (nomFallaSolucion, fallaSoln, idFalla)
						  values   ('Puertos USB no funcionan',true, null),
								   ('controladores desactualizados', false, 51),
                                   ('actualizar los controladores',false, 52),
                                   ('desgaste fisico por el uso constante',false, 51),	
                                   ('cortocircuito interno en el conector USB', false, 51),
                                   ('Errores en la gestion de energia de los puertos USB',false,51)
-- <AFM>



-- Nombre: Diana Gonzalez Almazan
-- <DGA>
insert into ArbolDeFallas values('No funciona el mouse', 1, null),
                                ('Verificar que no esté desactivado', 0, 57)
                                ('Reiniciar la laptop', 0, 57) 
                                ('Actualiza los controladores', 0, 57)
                                ('Revisar el hardware', 0, 57)
                                ('Ingresar al Administrador de Dispositivos', 0, 57)
                                ('Verificar que no tenga grasas, líquido o alimentos', 0, 57)
-- <DGA>



-- Nombre: Araceli Abrego Roman
-- <AAR>
insert into ArbolDeFallas values (46, 'Daño físico en componentes', true, null),
				                 (47, 'Reiniciar la PC', false, 31),
				                 (48, 'Realizar copias de seguridad regulares', false, 33),
				                 (49, 'Reemplazar hardware defectuoso', false, 41),
-- <AAR>



-- Nombre: Jared Rojas Adan
-- <JRA>
insert into ArbolDeFallas values  ('73','Problema de hardware interno',true,null),
                                  ('74','Comprobar la temperatura',false,73),
                                  ('75','Realizar una restauracion del sistema',false,73),
                                  ('76','Revision de la RAM',false,73),
                                  ('77','Verificar conexiones internas',false,73),
                                  ('78','Realizar pruebas de diagnóstico',false,73);
-- <JRA>



