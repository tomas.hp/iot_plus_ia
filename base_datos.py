import mysql.connector

def conectar():
    """
    Establece y devuelve una conexión a la base de datos MySQL.
    
    Returns:
    mysql.connector.connection.MySQLConnection: Objeto de conexión a la base de datos.
    """
    conexion = mysql.connector.connect(
        host="localhost",
        user="root",
        password="root",
        database="ejemplo",
        port='3306'
    )
    return conexion

def insertar_registro(tabla, valores):
    """
    Inserta un nuevo registro en la tabla especificada con los valores dados.
    
    Args:
    tabla (str): Nombre de la tabla en la que se insertará el registro.
    valores (dict): Diccionario de valores a insertar, donde las claves son los nombres de los campos y los valores son los valores a insertar.
    """
    conexion = conectar()
    cursor = conexion.cursor()
    campos = ', '.join(valores.keys())
    marcadores = ', '.join(['%s'] * len(valores))
    consulta = f"INSERT INTO {tabla} ({campos}) VALUES ({marcadores})"
    datos = tuple(valores.values())
    cursor.execute(consulta, datos)
    conexion.commit()
    cursor.close()
    conexion.close()

def eliminar_registro(tabla, condicion):
    """
    Elimina registros de la tabla que cumplan con la condición especificada.
    
    Args:
    tabla (str): Nombre de la tabla de la que se eliminarán registros.
    condicion (str): Condición para filtrar los registros a eliminar.
    """
    conexion = conectar()
    cursor = conexion.cursor()
    consulta = f"DELETE FROM {tabla} WHERE {condicion}"
    cursor.execute(consulta)
    conexion.commit()
    cursor.close()
    conexion.close()

def actualizar_valor(tabla, campo, valor_nuevo, condicion):
    """
    Actualiza el valor de un campo en registros que cumplan con la condición especificada.
    
    Args:
    tabla (str): Nombre de la tabla en la que se realizará la actualización.
    campo (str): Nombre del campo que se actualizará.
    valor_nuevo (str): Nuevo valor para el campo.
    condicion (str): Condición para filtrar los registros a actualizar.
    """
    conexion = conectar()
    cursor = conexion.cursor()
    consulta = f"UPDATE {tabla} SET {campo} = %s WHERE {condicion}"
    valor_nuevo = '' + valor_nuevo + '' if isinstance(valor_nuevo, str) else valor_nuevo
    cursor.execute(consulta, (valor_nuevo,))
    conexion.commit()
    cursor.close()
    conexion.close()

def consultar_registros(tabla):
    """
    Consulta y devuelve todos los registros de la tabla especificada.
    
    Args:
    tabla (str): Nombre de la tabla de la que se consultarán los registros.
    
    Returns:
    list: Lista de registros de la tabla.
    """
    conexion = conectar()
    cursor = conexion.cursor()
    consulta = f"SELECT * FROM {tabla}"
    cursor.execute(consulta)
    resultados = cursor.fetchall()
    cursor.close()
    conexion.close()
    return resultados

""" Ejemplo de uso
tabla = "ejemplo"
insertar_registro(tabla, {"clave": 3, "algo": "hola como estas compa"})
eliminar_registro(tabla, "clave=2")
actualizar_valor(tabla, "algo", "algo4sad", "clave=1")
registros = consultar_registros(tabla)
for registro in registros:
    print(registro)
"""